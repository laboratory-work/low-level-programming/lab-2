ASM=nasm
ASM_FLAG=-f elf64 -g
LD=ld
LD_FLAG=-o main

all: clean build

lib.o: lib.asm
	$(ASM) $(ASM_FLAG) -o lib.o lib.asm

dict.o: dict.asm lib.o
	$(ASM) $(ASM_FLAG) -o dict.o dict.asm

main.o: lib.o dict.o main.asm colon.inc words.inc
	$(ASM) $(ASM_FLAG) -o main.o main.asm

# %.o: %.asm
# 	$(ASM) $(ASM_FLAG) -o $@ $<

compile: lib.o dict.o main.o

build: compile
	$(LD) $(LD_FLAG) lib.o dict.o main.o

run: build
	./main

clean:
	rm -f *.o main