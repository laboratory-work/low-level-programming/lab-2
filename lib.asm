section .data
    buf times 256 db 0

section .text

; Принимает код возврата и завершает текущий процесс
global exit
exit: 
    mov     rax, 60          ; 'exit' syscall number
    xor     rdi, rdi
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rcx - счетчик
; нуль-термированная - это оканчивающаяся на 0x00
global string_length
string_length:
    xor rcx, rcx
    mov rax, rdi
    .loop:
        mov rax, [rdi + rcx]
        inc rcx
        test al, 0xFF
        jnz .loop
    dec rcx
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - input string
; rsi - stdout/stdin

; rax - системный вызов
; rdi - stdout
; rsi - строка
; rdx - длинна
global print_string
print_string:
    call string_length ; длинна
    push rsi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1      ; добавить разделение на stdout/stderr
    pop rdi
    syscall
    call print_newline
    ret


; Принимает код символа и выводит его в stdout
; rdi - символ печати
global print_char
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    push 0xA
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - число
global print_uint
print_uint:
    mov rax, rdi
    mov r8, 0x0A
    push 0x00
    .loop:
        xor rdx, rdx
        div r8          ; rax - частное, rdx - остаток
        add rdx, 0x30   ; '0' в ASCII таблице
        push rdx
        cmp rax, r8
        jae .loop
    add rax, 0x30
    cmp rax, 0x30
    je .next
    push rax

    .next:
        pop rdi
        cmp rdi, 0x00
        je .eof
        call print_char
        jmp .next
    .eof:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - число
global print_int
print_int:
    mov r8, rdi
    test rdi, rdi
    jns .print
    mov rdi, 0x2d
    call print_char
    mov rdi, r8
    neg rdi
    .print:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - первый
; rsi - второй
global string_equals
string_equals:
    xor rcx, rcx
    .loop:
        mov al, byte[rdi+rcx]
        mov dl, byte[rsi+rcx]
        cmp al,dl
        jne .neqls
        inc rcx
        cmp al, 0x00
        je .eqls
        jmp .loop
    .eqls:
        mov rax, 1
        ret
    .neqls:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; rdi - начало буфера
; rsi - размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0x10.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера. 
; Эта функция должна дописывать к слову нуль-терминатор
global read_word

; возвращает: в rax - слово, в rdx — длина слова
read_word:
    xor r8, r8
    mov r8, buf
    .skip:
        call read_char
        test rax, rax
        jz .finish
        cmp rax, 32
        jle .skip
    .read_next:
        mov [r8], rax
        inc r8
        call read_char
        cmp rax,  0x1f
        jg .read_next
    .finish:
        mov rdi, buf
        call string_length
        mov rdx, rax
        mov rax, buf
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global parse_uint
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor r8, r8
    mov r10, 0x0A
    .num:
        cmp byte[rdi+rcx], 0x30
        jb .not_correct
        cmp byte[rdi+rcx], 0x39
        ja .not_correct
        mul r10
        mov r8b, byte[rdi+rcx]
        sub r8b, 0x30
        add rax, r8
        inc rcx
        jmp .num
    .not_correct:
        mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global parse_int
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; rdi - указатель на строку
; rsi - указатель на буфер
; rdx - длинна буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
    push rdi
    push rdx

    call string_length
    cmp rdx, rax
    jle .carry
    
    pop rdx
    pop rdi
    push rdi
    push rdx
    .loop:
        xor rcx, rcx
        mov cl, byte[rdi]
        mov byte[rsi], cl
        inc rdi
        inc rsi
        test rcx, rcx    
        jnz .loop
    pop rdx
    pop rdi
    ret
    .carry:
        pop rdx
        pop rdi
        mov rax, 0
        ret
